// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use reqwest::{Body, Client, Method};
use reqwest::header::AUTHORIZATION;
use std::process::Command;

#[tauri::command]
async fn fetch_api_n(text: String, api_key: String){
    let client = Client::new();
    let mut resp = client.post("https://api-inference.huggingface.co/models/Hello-SimpleAI/chatgpt-detector-roberta")
        .header(AUTHORIZATION, "Bearer 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")
        .body(text);
    println!("Response Text: {:#?}", resp.unwrap());
}

async fn fetch_ai_curl(text: String, api_key: String){
    let output = if cfg!(target_os="windows"){
        Command::new()
    }
}

fn main() {
    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![fetch_api])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}

