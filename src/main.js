const { invoke } = window.__TAURI__.tauri;
async function query(data, usr_key) {
    let key = "Bearer ".concat(usr_key)
    const response = await fetch(
        "https://api-inference.huggingface.co/models/Hello-SimpleAI/chatgpt-detector-roberta",
        {
            headers: { Authorization: key },
            method: "POST",
            body: JSON.stringify(data),
        }
    );
    const result = await response.json();
    return result}
async function submit_on_click(){
    let html_input = document.getElementById("Input").value;
    let output = document.getElementById("output")
    output.innerHTML = "Judging"
    query(html_input,"Bearer PUT YOUR KEY HERE").then((response) => {

        output.innerHTML = response[0][0]["label"]
    });
}
